{{ config(materialized='incremental',unique_key='account_id',incremental_strategy='insert_overwrite') }}
with account as (
    select
         id as account_id,

         name as account_name

    from {{ env_var('db_name') }}.{{ env_var('db_schema') }}.account

), invoice as (

    select

        customerref__name,

        count(*) as pending_invoices

    from {{ env_var('db_name') }}.{{ env_var('db_schema') }}.invoices

    group by customerref__name

),customer_invoice as (

    select
        account_id,

        account_name,

        customerref__name,

        pending_invoices

    from account, invoice

    where account_name = customerref__name

    group by account_name,customerref__name,pending_invoices,account_id

),

salesforce_account_enhanced as (

    select
        account.account_id,
        customer_invoice.pending_invoices,
        CURRENT_TIMESTAMP as event_time,
        CURRENT_TIMESTAMP as update_time

    from account

    inner join customer_invoice using (account_id)

)

select * from salesforce_account_enhanced

{% if is_incremental() %}

   where update_time > (select max(update_time) from {{ this }})

{% endif %}