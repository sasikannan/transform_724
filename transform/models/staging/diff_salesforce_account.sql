{{ config(materialized='table', unique_key='account_id') }}

with account_enhanced as (

  SELECT account_id, pending_invoices
  from {{ref('salesforce_account_enhanced')}}

),

snapshot as (

  SELECT account_id, pending_invoices
  from {{ env_var('db_name') }}.{{ env_var('db_schema') }}.snapshot_salesforce_account

),

diff_snapshot as (

    select account_id,pending_invoices
    from account_enhanced
    EXCEPT
    select account_id,pending_invoices
    from snapshot

)

select * from diff_snapshot
