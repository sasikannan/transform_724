{{ config(materialized='table', unique_key='account_id') }}

with account_enhanced as (

  SELECT account_id, pending_invoices
  from {{ ref("diff_salesforce_account") }}

),

final as (

    select account_id,pending_invoices
    from account_enhanced

)

select * from final
